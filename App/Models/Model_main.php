<?php

namespace App\Models;


use App\Core\Model;

class Model_main extends Model
{

    public function get_data($table, $columns, $params, $limit, $fetch)
    {
        return parent::get_data($table, $columns, $params, $limit, $fetch);
    }

    public function generateposts($qposts, $qwords, $qusers)
    {
        return parent::generateposts($qposts, $qwords, $qusers);
    }
}
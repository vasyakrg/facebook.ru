<?php
/**
 * Copyright (c) 2018.
 */

/**
 * Created by PhpStorm.
 * User: vasyansk
 * Date: 2018-12-19
 * Time: 10:34
 */

namespace App\Core;


class Demo
{
    private $db;

    function __construct()
    {
        $this->db = Model::getInstance();
    }

    public function generateposts($qposts, $qwords, $qusers){
        $words = $this->words2array('Demo/words.txt');
        $iduser = 0;
        $post = '';
        $postdate = '';

        $idusers = array();
        for ($i=0; $i<$qposts; $i++){
            $idusers[$i] = rand($qusers, 1);
        }

        $posts = array();
        for ($i=0; $i<$qposts; $i++){
            $posts[$i] = $this->getrandomwords($words, $qwords);
        }

//        $result = $this->db->prepare('insert into posts (iduser, post, postdate) values (:iduser, :post, :postdate)');
//        $result->bindParam('iduser', $iduser);
//        $result->bindParam(':post', $post);
//        $result->bindParam(':postdate', $postdate);
        for ($i=0; $i<$qposts; $i++){
            $iduser = $idusers[$i];
            $post = $posts[$i];
            $postdate = date('Y-m-d H:i:s' , rand(strtotime('-1 year'), time()));
            $params = array(
              'iduser' => $iduser,
              'post' => $post,
              'postdate' => $postdate
            );
            $this->db->insert_data('posts', $params);
//            $result->execute();
        }
        return true;
    }

    public function words2array($file){
        $words = file($file);
        for ($i=0; $i<count($words); $i++){
            $words[$i] = str_replace("\r\n", NULL, $words[$i]);
        }
        return $words;
    }

    /**
     * Генерит строку размером $qty из случайных слов на основе массива слов $arraywords, разделяя слова пробелом
     * @param $arraywords
     * @param $qty
     * @return string
     */
    public function getrandomwords($arraywords, $qty){
        $str ='';
        for ($i=0; $i<=$qty; $i++) {
            $rand_keys = array_rand($arraywords);
            $str .= $arraywords[$rand_keys] . ' ';
        }
        return trim($str);
    }
}
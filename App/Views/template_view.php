<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
Design by Free CSS Templates
http://www.freecsstemplates.org
Released for free under a Creative Commons Attribution 3.0 License

Name       : Accumen
Description: A two-column, fixed-width design with dark color scheme.
Version    : 1.0
Released   : 20120712

Modified by VitalySwipe
-->
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <link rel="shortcut icon" href="/favicon.ico" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<title>Facebook 3.0</title>
		<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css" />
		<link href="http://fonts.googleapis.com/css?family=Kreon" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="/css/style.css" />
	</head>
	<body>
		<div id="wrapper">
			<div id="header">
				<div id="logo">
					<a href="/">Facebook 3.0</span> <span class="cms"></span></a>
				</div>
				<div id="menu">
					<ul>
						<li class="first active"><a href="/">Главная</a></li>
						<li><a href="/about">О программе</a></li>
						<li class="last"><a href="/contacts">Контакты</a></li>
					</ul>
					<br class="clearfix" />
				</div>
			</div>
    <div id="page">
        <div id="sidebar">
            <div class="side-box">
                <h3>Основное меню</h3>
                <ul class="list">
                    <li class="first "><a href="/">Главная</a></li>
                    <li><a href="/about">О программе</a></li>
                    <li class="last"><a href="/contacts">Контакты</a></li>
                </ul>
            </div>
<?php if ($sess) {?>
             <div class="side-box">
                    <h3>Личное меню</h3>
                        <ul class="list">
                            <li class="first "><a href="/profile">Профиль</a></li>
                            <li><a href="/myposts">Мои посты</a></li>
                            <li><a href="/newpost">Новый пост</a></li>
                            <li><a href="/logout">Выйти</a></li>
                        </ul>

            </div>
					<?php }
					else { ?>
            <div class="side-box">
                        <h3>Личное меню</h3>
                            <ul class="list">
                                <li class="first "><a href="/auth">Авторизация</a></li>
                                <li class="first "><a href="/register">Регистрация</a></li>
                            </ul>

            </div>
                    <?php
                    }
                    ?>
        </div>

				<div id="content">
					<div class="box">
						<?php include 'App/Views/'.$content_view; ?>
						<!--
						<h2>Welcome to Accumen</h2>
						<img class="alignleft" src="images/pic01.jpg" width="200" height="180" alt="" />
						<p>
							This is <strong>Accumen</strong>, a free, fully standards-compliant CSS template by <a href="http://www.freecsstemplates.org/">Free CSS Templates</a>. The images used in this template are from <a href="http://fotogrph.com/">Fotogrph</a>. This free template is released under a <a href="http://creativecommons.org/licenses/by/3.0/">Creative Commons Attributions 3.0</a> license, so you are pretty much free to do whatever you want with it (even use it commercially) provided you keep the footer credits intact. Aside from that, have fun with it :)
						</p>
						-->
					</div>
					<br class="clearfix" />
				</div>
				<br class="clearfix" />
			</div>
			<div id="page-bottom">
				<div id="page-bottom-sidebar">
					<h3>Наши контакты</h3>
					<ul class="list">
						<li class="first">Nsk</li>
						<li>Vassiliy Yegorov</li>
						<li class="last">email: vasyakrg@gmail.com</li>
					</ul>
				</div>
				<div id="page-bottom-content">
					<h3>Подвал</h3>
					<p>какие данные, айпи адрес и что-то еще</p>
				</div>
				<br class="clearfix" />
			</div>
		</div>
		<div id="footer">
			<a href="/">Facebook 3.0</a> &copy; 2019</a>
		</div>
	</body>
</html>
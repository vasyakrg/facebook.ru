<?php
namespace App\Controllers;

use App\Core\Controller;

class Controller_contacts extends Controller
{
    function action_index()
	{
        $auth = new Controller_auth();
        if ($auth->getssesion())
            $this->view->generate('contacts_view.php', 'template_view.php', null, true);
        else $this->view->generate('contacts_view.php', 'template_view.php', null, false);
	}
}

<?php

namespace App\Controllers;


use App\Core\Controller;
use App\Core\Model;
use App\Core\View;

class Controller_newpost extends Controller
{
    function __construct()
    {
        $this->model = new Model();
        $this->view = new View();
    }

    public function action_index()
    {
        session_start();
        $auth = new Controller_auth();

         if (isset($_POST['newpost'])) {
             echo 'newpost!';
             $iduser = $auth->getidfromlogin($_SESSION['user']);
             $postdate = date('Y-m-d H:i:s');
             $post = $_POST['newpost'];
             $params = array(
                 'iduser' => $iduser,
                 'post' => $post,
                 'postdate' => $postdate,
             );
             $this->model->insert_data('posts', $params);
             $this->view->generate('profile_view.php', 'template_view.php', null, true);
         }

         if ($auth->getssesion()) {
            $this->view->generate('newpost_view.php', 'template_view.php', null, true);
        }
        else {
            $this->view->generate('auth_view.php', 'template_view.php', null, false);
        }
    }
}
<?php

namespace App\Controllers;


use App\Core\Controller;
use App\Core\Model;
use App\Core\View;

class Controller_profile extends Controller
{
    public function __construct()
    {
        $this->model = new Model();
        $this->view = new View();
    }

    public function action_index()
    {
        $auth = new Controller_auth();
        if ($auth->getssesion()) {
            session_start();
            $data = array(
              'login' => $_SESSION['user'],
            );
            $this->view->generate('profile_view.php', 'template_view.php', $data, true);
        }
        else {
            $data = array(
                'login' => 'вы не авторизованы',
            );
            $this->view->generate('auth_view.php', 'template_view.php', $data, false);
        }
    }
}
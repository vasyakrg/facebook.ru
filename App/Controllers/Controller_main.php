<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Core\Demo;
use App\Core\View;
use App\Models\Model_main;

class Controller_main extends Controller
{

    function __construct()
    {
        $this->model = new Model_main();
        $this->view = new View();
        $this->demo = new Demo();
    }

	function action_index()
	{
	    session_start();
        $auth = new Controller_auth();
	    $columns = array(
            '1' => 'post',
            '2' => 'postdate',
            '3' => 'iduser',
        );

        if ($auth->getssesion())
             $sess = true; else $sess = false;

        $data = $this->model->get_data('posts', $columns, null, 10 , null);
        $this->view->generate('main_view.php', 'template_view.php', $data, $sess);
	}

	public function action_params($params)
    {
        echo "заглушка метода <a href='index.php'>на главную";
        {// for DEMO only
//            $this->demo->generateposts($params['postgen'], 5, 5);
        }
    }
}
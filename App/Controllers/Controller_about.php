<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Core\View;

class Controller_about extends Controller
{

	function __construct()
	{
		$this->view = new View();
	}
	
	function action_index()
	{
        $auth = new Controller_auth();
        if ($auth->getssesion())
            $sess = true; else $sess = false;
		$this->view->generate('about_view.php', 'template_view.php', null, $sess);
	}
}

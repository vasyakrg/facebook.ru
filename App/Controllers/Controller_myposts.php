<?php

namespace App\Controllers;


use App\Core\Controller;
use App\Core\Model;

class Controller_myposts extends Controller
{

    public function action_index()
    {
        $auth = new Controller_auth();
        $model = new Model();
        if ($auth->getssesion()) {
            $columns = array(
                '1' => 'post',
                '2' => 'postdate'
            );
            session_start();
            $iduser = $auth->getidfromlogin($_SESSION['user']);
            $params = array(
              'iduser' => $iduser,
            );

            $data = $model->get_data('posts', $columns, $params, 10, null);
            $this->view->generate('myposts_view.php', 'template_view.php', $data, true);
        }
        else $this->view->generate('auth_view.php', 'template_view.php', null, false);
    }
}